import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    posts:[],
    authors:[],
  },
  mutations: {
      setPosts(state,payload){
          state.posts = payload
      },
      setAuthors(state,payload){
          payload.forEach((authorElement) => {
              let {name, id} = authorElement
              state.authors.push({name, id})
          })
      }
  },
  getters: {
    getPosts: state => state.posts,
    getAuthors: state => state.authors,
    getAuthor: (state) => (id) => {
        return state.authors.find(author => author.id === id).name
    }
  },
  actions: {
    receivePost({ commit }){
      axios.get('https://jsonplaceholder.typicode.com/posts').then(
          ((response) => {
              commit('setPosts', response.data)
          })
      )
    },
    receiveAuthors({ commit }){
      axios.get('https://jsonplaceholder.typicode.com/users').then(
          ((response) => {
              commit('setAuthors', response.data)
          })
      )
    },
  }
})
